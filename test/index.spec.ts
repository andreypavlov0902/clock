

import jasmine from 'jasmine';


jasmine.describe("create and initialize class Main", function () {

    let myClass;
    jasmine.beforeEach(function () {
        let ukr = {
            selector: '#entry-template',
            addSelector: '.format1',
            subscribeSelector: '#date',
            clockSelector: '.chart1',
            timeZone: 3,
            locale: '',
            location: 'Ukraine'
        };
        document.body.innerHTML = ` <input type="text" id="date" class="my-date">
        <div>
            <div class="chart1"></div>
            <p class="format1"></p>
        </div>`;
/*        myClass = new main(ukr);
        myClass.inputData = document.createElement('input')
        spyOn(myClass, 'timeZone');
        spyOn(myClass, 'performSubscribe');*/
    });

    jasmine.it("should run the function", function () {
        jasmine.expect(myClass.timeZone).toHaveBeenCalled();
    });

/*    it("should multiply 0 and 3 to 0", function () {
        expect(index.Multiply(0, 3)).toBe(0);
    });*/
});