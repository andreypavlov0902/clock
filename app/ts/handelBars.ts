/**
 * Created by andrei on 09.07.2017.
 */
import * as handlebars from 'handlebars';
import * as $ from 'jquery';
export class handelBars {
    private completeTemplate;
    private handleBars: handlebars;
    constructor(handlebars, selector) {
        this.handleBars = handlebars;
        this.template();
        this.compileTemplate(selector);
    }
    public addTemplate (data, selector) {
        if( this.searchAddTemplate(selector)) {
            this.searchAddTemplate(selector).append(this.completeTemplate(data));
        }
    }
    private searchAddTemplate (selector) {
        return $(selector);
    }
    private serchTemplate(selector) {
        return $(selector).html();
    }
    private compileTemplate(selector) {
        if(this.serchTemplate(selector)) {
            this.completeTemplate = this.handleBars.compile(this.serchTemplate(selector));
        }
    }
    private template() {
        this.handleBars.registerHelper('list', (items, options) => {
            let out = "<ul>";
            if(items) {
                for(let item of items ) {
                    out = out + "<li>" + options.fn(item) + "</li>";
                }
            }
            return out + "</ul>";
        });
    }
}