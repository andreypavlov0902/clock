/**
 * Created by andrei on 07.07.2017.
 */

import * as d3 from "d3";
import * as moment from 'moment';
import * as $ from 'jquery';
import * as handlebars from 'handlebars';
import { handelBars } from './handelBars'
import { Clock } from './clock'
import { MomentComponent } from './moment';
import '../sass/style.scss';


export class Main {
    public stopTime;
    private hb;
    private moment;
    private inputData;
    private addBlock;
    private zone: number = 0;
    private addSelector;
    private  location;
    private subscribeSelector: string;
    constructor({selector, addSelector, subscribeSelector, clockSelector, timeZone, locale, location}) {
        this.hb = new handelBars(handlebars, selector);
        this.addSelector = addSelector;
        this.subscribeSelector = subscribeSelector;
        this.location =  location;
        this.timeZone(timeZone);
        this.moment = new MomentComponent(moment, locale);
        this.addBlock = this.querySelector(addSelector);
        this.inputData = this.querySelector(subscribeSelector);
        this.performSubscribe();
        new Clock(d3, clockSelector, timeZone);
    }
    public set time(value) {
        this.moment.globalTime = value;
    }
    public run() {
        this.addBlock.innerHTML = '';
        this.stopTime = setTimeout(() => {
            this.run();
        }, 1000);
        this.hb.addTemplate(this.returnData(), this.addSelector);
    }
    private querySelector(selector: string) {
        return <HTMLElement>document.querySelector(selector);
    }
    public performSubscribe() {
        this.inputData.addEventListener('blur', (event) => {
            this.time = (<HTMLInputElement>event.target).value;
            this.addBlock.innerHTML = '';
            this.run();
        });
    }
    private timeZone(val: number) {
        this.zone = (val <= 12 && val >= -12) ? val : 0;
    }
    private returnData() {
        return {
            location:  this.location,
            date: [
                    {now: 'Now data', time: this.moment.notFormatDate(this.zone)},
                    {now: 'The format Date', time: this.moment.formatDate(this.zone) },
                    {now: 'Calendar Time', time: this.moment.calendarDate(this.zone) },
                ]
        }
    }

}

let ukr = {
        selector: '#entry-template',
        addSelector: '.format1',
        subscribeSelector: '#date',
        clockSelector: '.chart1',
        timeZone: 3,
        locale: '',
        location: 'Ukraine'
};
let rus = {

        selector: '#entry-template',
        addSelector: '.format2',
        subscribeSelector: '#date',
        clockSelector: '.chart2',
        timeZone: 2,
        locale: '',
        location: 'Russia'
};
let can = {
        selector: '#entry-template',
        addSelector: '.format3',
        subscribeSelector: '#date',
        clockSelector: '.chart3',
        timeZone: -7,
        locale: '',
        location: 'Canada'
};
let myMain = new Main(ukr);
myMain.run();

let my3 = new Main(rus);
my3.run();

let my4 = new Main(can);
my4.run();


