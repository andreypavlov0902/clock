/**
 * Created by andrei on 09.07.2017.
 */

export class MomentComponent {
    private moment;
    private time = null;
    constructor(moment, locale?) {
        /*moment.locale('ru')*/
        this.moment = moment;
        this.time = new Date();
        this.moment.locale( locale) ;
    }
    private get getTime() {
        return new Date();
    }
    public notFormatDate(zone: number) {
       return this.moment(this.getTime).utcOffset(zone).format();
    }
    public formatDate(zone: number) {
       return this.moment(new Date()).utcOffset(zone).format('MMMM Do YYYY, h:mm:ss a');
    }
    public calendarDate(zone: number) {
        return this.moment(this.globalTime).utcOffset(zone).calendar()
    }
    public get globalTime () {
        return this.time;
    }
    public set globalTime (value) {
        this.time = value ? value : new Date();
    }
}