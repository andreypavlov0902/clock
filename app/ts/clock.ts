/**
 * Created by andrei on 08.07.2017.
 */
/*import * as d3 from "d3";*/
export class Clock {
    private selector: string;
    private d3;
    private timeZone: number;
    private render;
    constructor(d3, selector, timeZone?) {
        this.selector = selector;
        this.d3 = d3;
        this.setAny();
        this.run();
        this.timeZone = timeZone ? timeZone : 0;
    }
    private setTimeObj() {
        let currentTime, hour, minute, second;
        currentTime = new Date();
        second = currentTime.getSeconds();
        minute = currentTime.getMinutes();
        hour = currentTime.getHours() + this.timeZone + minute / 60;
        return  [
            {
                "unit": "seconds",
                "numeric": second
            }, {
                "unit": "minutes",
                "numeric": minute
            }, {
                "unit": "hours",
                "numeric": hour
            }
        ];
    }
    private setAny(){
        let width, height, offSetX, offSetY, pi, scaleSecs, scaleHours, scaleMins;
        width = 400;
        height = 200;
        offSetX = 150;
        offSetY = 100;

        pi = Math.PI;
        scaleSecs =  this.d3.scaleLinear().domain([0, 59 + 999/1000]).range([0, 2 * pi]);
        scaleMins =  this.d3.scaleLinear().domain([0, 59 + 59/60]).range([0, 2 * pi]);
        scaleHours = this.d3.scaleLinear().domain([0, 11 + 59/60]).range([0, 2 * pi]);
        let vis, clockGroup;

        vis = this.d3.selectAll(this.selector)
            .append("svg:svg")
            .attr("width", width)
            .attr("height", height);

        clockGroup = vis.append("svg:g")
            .attr("transform", "translate(" + offSetX + "," + offSetY + ")");
        clockGroup.append("svg:circle")
            .attr("r",84).attr("fill", "none").attr("stroke-width",6).attr("stroke", "gray");
        clockGroup.append("svg:circle")
            .attr("r",75).attr("fill", "none").attr("stroke-width",11).attr("stroke", "black")
            .attr("stroke-dasharray", 4,40.789082).attr("transform", "rotate(-1.5)");
        clockGroup.append("svg:circle")
            .attr("r",75).attr("fill", "none").attr("stroke-width",5).attr("stroke", "black")
            .attr("stroke-dasharray", 2,8.471976).attr("transform", "rotate(-.873)");
        clockGroup.append("svg:circle")
            .attr("r", 80).attr("fill", "none")
            .attr("class", "clock outercircle")
            .attr("stroke", "black")
            .attr("stroke-width", 2);

        clockGroup.append("svg:circle")
            .attr("r", 4)
            .attr("fill", "black")
            .attr("class", "clock innercircle");


       this.render = function(data) {

            var hourArc, minuteArc, secondArc;

            clockGroup.selectAll(".clockhand").remove();

            secondArc = this.d3.arc()
                .innerRadius(0)
                .outerRadius(70)
                .startAngle(function(d) {
                    return scaleSecs(d.numeric);
                })
                .endAngle(function(d) {
                    return scaleSecs(d.numeric);
                });

            minuteArc = this.d3.arc()
                .innerRadius(0)
                .outerRadius(70)
                .startAngle(function(d) {
                    return scaleMins(d.numeric);
                })
                .endAngle(function(d) {
                    return scaleMins(d.numeric);
                });

            hourArc = this.d3.arc()
                .innerRadius(0)
                .outerRadius(50)
                .startAngle(function(d) {
                    return scaleHours(d.numeric % 12);
                })
                .endAngle(function(d) {
                    return scaleHours(d.numeric % 12);
                });

            clockGroup.selectAll(".clockhand")
                .data(data)
                .enter()
                .append("svg:path")
                .attr("d", function(d) {
                    if (d.unit === "seconds") {
                        return secondArc(d);
                    } else if (d.unit === "minutes") {
                        return minuteArc(d);
                    } else if (d.unit === "hours") {
                        return hourArc(d);
                    }
                })
                .attr("class", "clockhand")
                .attr("stroke", "black")
                .attr("stroke-width", function(d) {
                    if (d.unit === "seconds") {
                        return 2;
                    } else if (d.unit === "minutes") {
                        return 3;
                    } else if (d.unit === "hours") {
                        return 3;
                    }
                })
                .attr("fill", "none");
        };
    }
    public run() {
        setInterval(() => {
            let data;
            data = this.setTimeObj();
            return this.render(data);
        }, 1000);
    }
}
