/*let webpack = require('webpack');
let path = require('path');
let ExtractTextPlugin = require("extract-text-webpack-plugin");
let CopyWebpackPlugin = require('copy-webpack-plugin');
let HtmlWebpackPlugin = require('html-webpack-plugin');
module.exports = {
  context: path.resolve(__dirname, './app'),
    devServer: {
        contentBase: path.resolve(__dirname, 'dist'), // New line
    },
  entry: './ts/main.ts',
    output: {
      filename: '[name].bundle.js',
      path: path.resolve(__dirname, 'dist'),
      publicPath: './',
      sourceMapFilename: '[file].map',
  },
    resolve: {
        // Add '.ts' and '.tsx' as resolvable extensions.
        extensions: [".webpack.js", ".web.js", ".ts", ".tsx", ".js"]
    },
    module: {
        loaders: [
            // all files with a '.ts' or '.tsx' extension will be handled by 'ts-loader'
            { test: /\.tsx?$/, loader: "ts-loader" }
        ],
        rules: [

           {
                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: ['css-loader',
                   {
                            loader: 'postcss-loader',
                            options: {
                                plugins: function () {
                                    return [require('autoprefixer')];
                                },
                            },
                        },
                        'sass-loader']
                })
            },
        ]
    },

    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            "Tether": 'tether'
        }),
        new ExtractTextPlugin("styles.css"),
        new CopyWebpackPlugin([
            {from: 'json/!**!/!*'},
        ]),
        new CopyWebpackPlugin([
            {from: 'css/!**!/!*'},
        ]),
        new HtmlWebpackPlugin({
            title: 'Index-html',
            filename: 'index.html',
            template: 'index.html',
            chunks: ['main']
        }),
    ]
};*/
let webpack = require('webpack');
var path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
var CopyWebpackPlugin = require('copy-webpack-plugin');
var HtmlWebpackPlugin = require('html-webpack-plugin');
module.exports = {
    context: path.resolve(__dirname, './app'), // New line
    devServer: {
        contentBase: path.resolve(__dirname, 'dist'), // New line
    },
    entry: {
        main: './ts/main.ts',
    },
    output: {
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, 'dist'),
        publicPath: './',
        sourceMapFilename: '[file].map',

    },
    resolve: {
        // Add '.ts' and '.tsx' as a resolvable extension.
        extensions: [ ".webpack.js", ".web.js", ".ts", ".tsx", ".js"],

    },
    node: {
        fs: "empty"
    },
    module: {

        rules: [
            {
             test: /\.tsx?$/,
             use :["ts-loader"]
            },
            { test: /\.hbs$/, use:["handlebars-loader"]  },
            {
                test: /\.css$/,
                use: [
                    'style-loader',
                    'css-loader',
                ],
            },
            {
                test: /\.(jpg|jpeg|png|svg)$/,
                use: ['url-loader'],
            },
            {

                test: /\.scss$/,
                use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: ['css-loader',
                        {
                            loader: 'postcss-loader',
                            options: {
                                plugins: function () {
                                    return [require('autoprefixer')];
                                },
                            },
                        },
                        'sass-loader'
                    ]
                })
            },
        ]
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            "Tether": 'tether'
        }),
        new CopyWebpackPlugin([
            {from: 'json/**/*'},
        ]),
        new ExtractTextPlugin("[name]/styles.css"),
        new CopyWebpackPlugin([
            {from: 'css/**/*'},
        ]),
        new CopyWebpackPlugin([
            {from: 'libs/**.*'},
        ]),
        new CopyWebpackPlugin([
            {from: 'template/**.*'},
        ]),
        new HtmlWebpackPlugin({
            title: 'Index-html',
            filename: 'index.html',
            template: 'index.html',
            chunks: ['main']
        }),
    ]
}
