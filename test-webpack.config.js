var webpack = require('webpack');
var path = require('path');
const ExtractTextPlugin = require("extract-text-webpack-plugin");
module.exports = {
  devtool: '#inline-source-map',

  entry: [
    './test/index.spec.ts',
  ],
  output: {
    filename: 'dist/bundle.js'
  },


  plugins: [
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.LoaderOptionsPlugin({
      debug: true
    })
  ],

  resolve: {
    extensions: ['.ts', '.js', '.tsx']
  },

  module: {
    rules: [
      {
        test: /\.ts$/, use: [{
          loader: 'ts-loader'
        }]
      },
        {
            test: /\.tsx?$/,
            use :["ts-loader"]
        },

        {

            test: /\.scss$/,
            use: ExtractTextPlugin.extract({
                fallback: "style-loader",
                use: ['css-loader',
                    {
                        loader: 'postcss-loader',

                    },
                    'sass-loader'
                ]
            })
        },
      ]
  }
};