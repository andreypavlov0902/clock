download files from a repository.
come to the folder with the project and set dependences:

```
#!python

npm install
```


2. after installation of dependences launch the server:

```
#!python

npm run dev
```


3. transfer in the browser on the way specified in the console:
by default 

```
#!python

http://localhost:8080/
```